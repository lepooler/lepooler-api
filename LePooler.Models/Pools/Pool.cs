﻿using LePooler.Models.Players;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Serialization;

namespace LePooler.Models.Pools
{
    public class Pool
    {
        [Key] public Guid PoolGuid { get; set; }
        public string Name { get; set; }
        public string Season { get; set; }
        public int NbGoalerPicks { get; set; }
        public int NbOffenseDefensePicks { get; set; }
        public int PickTimeLimitInSeconds { get; set; }

        [JsonIgnore]
        public string Salt { get; set; }

        [JsonIgnore]
        public byte[] HashedPassword { get; set; }

        [JsonIgnore]
        public ICollection<PoolParticipant> PoolParticipants { get; set; }

        [JsonIgnore]
        public Pick LastPick { get; private set; }

        [JsonIgnore]
        [NotMapped]
        private ICollection<PoolParticipant> PoolersInPickOrder { get; set; }

        [JsonIgnore]
        [NotMapped]
        public string Password
        {
            get => _password;
            set => _password = value;
        }

        [JsonPropertyName("password")]
        [NotMapped]
        public string PasswordSetter { set => _password = value; }

        [JsonIgnore]
        private string _password;

        [JsonIgnore]
        [NotMapped]
        public IEnumerable<Pick> Picks => PoolParticipants.SelectMany(p => p.Picks);

        public Pool()
        {
            PoolParticipants = new List<PoolParticipant>();
        }

        private byte[] GetSaltedAndHashedString(string stringToSaltAndHash)
        {
            var saltedPassword = stringToSaltAndHash + Salt;
            SHA512 shaM = new SHA512Managed();
            return shaM.ComputeHash(Encoding.UTF8.GetBytes(saltedPassword));
        }

        public void HashPassword()
        {
            if (HashedPassword != null && HashedPassword.Length != 0)
                return;

            if (string.IsNullOrEmpty(Salt))
                Salt = Guid.NewGuid().ToString();
            HashedPassword = GetSaltedAndHashedString(Password);
        }

        public bool PasswordMatches(string password)
        {
            return GetSaltedAndHashedString(password).SequenceEqual(HashedPassword);
        }

        public int GetTotalNumberOfPicks()
        {
            return (NbGoalerPicks + NbOffenseDefensePicks) * PoolParticipants.Count;
        }

        public int GetNumberOfRemainingPicks()
        {
            return GetTotalNumberOfPicks() - PoolParticipants.SelectMany(p => p.Picks).Count();
        }

        public ICollection<PoolParticipant> GetPoolParticipantsInPickOrder()
        {
            return PoolersInPickOrder ??= PoolParticipants.OrderBy(p => p.PickOrderPosition).ToList();
        }

        public PoolParticipant GetNextPoolParticipantToPick()
        {
            if (GetNumberOfRemainingPicks() <= 0)
            {
                return null;
            }

            var nbPicks = PoolParticipants.SelectMany(p => p.Picks).Count();
            var pickOrderAscending = nbPicks / PoolParticipants.Count % 2 == 0;
            var pickingPosition = pickOrderAscending ?
                nbPicks % PoolParticipants.Count + 1 :
                PoolParticipants.Count - nbPicks % PoolParticipants.Count;

            return GetPoolParticipantsInPickOrder().Single(x => x.PickOrderPosition == pickingPosition);
        }

        public IEnumerable<Player> GetPoolerPicks(Guid poolerGuid)
        {
            return PoolParticipants.Single(p => p.PoolerGuid == poolerGuid).Picks.Select(p => p.Player);
        }

        public Pick PickPlayer(Player player)
        {
            var nextPoolParticipantToPick = GetNextPoolParticipantToPick();
            if (nextPoolParticipantToPick == null)
                throw new InvalidOperationException("Tous les choix ont été faits");

            if (player.Position == PlayerPosition.Goaler && Picks.Count(p =>
                    p.Player.Position == PlayerPosition.Goaler &&
                    p.PoolerGuid == nextPoolParticipantToPick.PoolerGuid) >= NbGoalerPicks)
                throw new InvalidOperationException("Ce participant à déjà choisi tous ses gardiens");

            if ((player.Position == PlayerPosition.Offense || player.Position == PlayerPosition.Defense) && Picks.Count(
                    p => (p.Player.Position == PlayerPosition.Offense || p.Player.Position == PlayerPosition.Defense) &&
                         p.PoolerGuid == nextPoolParticipantToPick.PoolerGuid) >= NbOffenseDefensePicks)
                throw new InvalidOperationException("Ce participant à déjà choisi tous ses attaquants/défenseurs");

            if (Picks.Any(p => p.PlayerNhlId == player.PlayerNhlId))
                throw new InvalidOperationException("Ce joueur a déjà été choisi par un autre participant");

            var newPick = new Pick
            {
                PlayerNhlId = player.PlayerNhlId,
                PoolGuid = PoolGuid,
                PoolerGuid = nextPoolParticipantToPick.PoolerGuid
            };

            PoolParticipants.Single(p => p.PoolerGuid == newPick.PoolerGuid).Picks.Add(newPick);
            LastPick = newPick;
            return newPick;
        }

        public void UndoLastPick()
        {
            if (!PoolParticipants.Single(p => p.PoolerGuid == LastPick.PoolerGuid).Picks.Remove(LastPick))
                throw new InvalidOperationException("Le dernier choix n'a pas pû être annulé");

            LastPick = null;
        }
    }
}