﻿using LePooler.Models.Players;
using System;

namespace LePooler.Models.Pools
{
    public class Pick
    {
        public Guid PoolGuid { get; set; }
        public Guid PoolerGuid { get; set; }

        public string PlayerNhlId { get; set; }
        public Player Player { get; set; }

        public PoolParticipant PoolParticipant { get; set; }
    }
}