﻿using LePooler.Models.Poolers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LePooler.Models.Pools
{
    public class PoolParticipant
    {
        [Key]
        public Guid PoolGuid { get; set; }

        public Pool Pool { get; set; }

        [Key]
        public Guid PoolerGuid { get; set; }

        public Pooler Pooler { get; set; }

        public int PickOrderPosition { get; set; }

        [JsonIgnore]
        public ICollection<Pick> Picks { get; set; }

        public PoolParticipant()
        {
            Picks = new List<Pick>();
        }
    }
}