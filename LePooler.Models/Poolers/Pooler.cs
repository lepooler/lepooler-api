﻿using LePooler.Models.Pools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LePooler.Models.Poolers
{
    public class Pooler
    {
        [Key]
        public Guid PoolerGuid { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<PoolParticipant> Pools { get; set; }

        public Pooler()
        {
            Pools = new List<PoolParticipant>();
        }
    }
}