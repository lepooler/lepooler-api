﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LePooler.Models.Players
{
    public class Player
    {
        [Key]
        public string PlayerNhlId { get; set; }

        public PlayerPosition Position { get; set; }
        public string Name { get; set; }
        public string Team { get; set; }
        public string TeamAbbreviation { get; set; }
#pragma warning disable CA1056 // Uri properties should not be strings
        public string TeamLogoUrl { get; set; }
#pragma warning restore CA1056 // Uri properties should not be strings
        public int Age { get; set; }
        public string Height { get; set; }
        public int Weight { get; set; }
        public double Salary { get; set; }

        public ICollection<PlayerPredictions> Predictions { get; set; } = new List<PlayerPredictions>();
        public ICollection<Injury> Injuries { get; set; } = new List<Injury>();
    }
}