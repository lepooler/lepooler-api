﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LePooler.Models.Players
{
    public class PlayerPredictions
    {
        [Key]
        public string PlayerNhlId { get; set; }

        [Key]
        public string Season { get; set; }

        [JsonIgnore]
        public Player Player { get; set; }

        public int Rank { get; set; }
        public int? GamesPlayed { get; set; }
        public int? Goals { get; set; }
        public int? Assists { get; set; }
        public int? Victories { get; set; }
        public int? Defeats { get; set; }
        public int? Shutouts { get; set; }
        public int? OvertimeDefeats { get; set; }
        public int? VictoriesLastYear { get; set; }
        public int? DefeatsLastYear { get; set; }
        public int? ShutoutsLastYear { get; set; }
        public int? OvertimeDefeatsLastYear { get; set; }
        public int? Points { get; set; }
        public double? PointsPerMatch { get; set; }
        public int? GamesPlayedLastYear { get; set; }
        public int? GoalsLastYear { get; set; }
        public int? AssistsLastYear { get; set; }
        public int? PointsLastYear { get; set; }
    }
}