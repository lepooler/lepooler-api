﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LePooler.Models.Players
{
    public class Injury
    {
        [Key]
        public string PlayerNhlId { get; set; }

        [Key]
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [JsonIgnore]
        public Player Player { get; set; }

        public string Description { get; set; }
    }
}