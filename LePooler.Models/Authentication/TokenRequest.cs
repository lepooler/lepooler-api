﻿namespace LePooler.Models.Authentication
{
    public class TokenRequest
    {
        public string PoolName { get; set; }
        public string Season { get; set; }
        public string Password { get; set; }
    }
}