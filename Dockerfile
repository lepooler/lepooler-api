FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine

WORKDIR /app
COPY /publish .

EXPOSE 2846/tcp
ENTRYPOINT ["dotnet", "LePooler.API.dll"]