﻿using System;

namespace LePooler.Scrapers.Outputs
{
    public class ScrapedInjury
    {
        public string PlayerName { get; set; }
        public string TeamName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
    }
}