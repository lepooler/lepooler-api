﻿using LePooler.Scrapers.Outputs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LePooler.Scrapers.Interfaces
{
    public interface IInjuriesScraper
    {
        Task<IEnumerable<ScrapedInjury>> GetInjuries();
    }
}