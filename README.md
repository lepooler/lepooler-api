# lepooler-api

Backend for [LePooler-UI](https://gitlab.com/lepooler/lepooler-ui).

The application allows creating NHL pools.

## Requirements
- A MySql or MariaDB instance running.
- A predictions file is required to load players in the database. A sample file containing outdated data is available in this repository : `predictions-sample.json`.

## Using Docker
### Environment variables
|Name|Required|Default <br/>(if not required)|Description|
|:-:|:-:|:-:|---|
|CORS_ALLOWED_ORIGINS|X||List of comma-separated origins to allow when setting up CORS. <br/>Example : `https://lepooler.example.com, http://lepooler.example.com`|
|JWT_VALID_ISSUERS|X||List of comma-separated domains that can issue JWTs for your instance of the application. This is usually the API host. <br/>Example : `lepoolerapi.example.com`|
|JWT_VALID_AUDIENCES|X||List of comma-separated domains for which the application issues JWTs. This is usually the [LePooler-UI](https://gitlab.com/lepooler/lepooler-ui) host. <br/>Example : `lepooler.example.com`|
|JWT_SIGNING_KEY|X||Key used to sign issued JWTs. This can be a GUID that you can [generate online](https://www.guidgenerator.com/).|
|MYSQL_HOST|X||Database host address. <br/>Example: `192.168.0.100`|
|MYSQL_PORT||`3306`|Port for the database connection on `MYSQL_HOST`.|
|MYSQL_DATABASE||`lepooler`|Database used to store LePooler data.|
|MYSQL_USER||`lepooler`|Username to connect to the database.|
|MYSQL_PASSWORD|X||Password to connect to the database.|
|PREDICTIONS_FILE_LOCATION||`/data/predictions.json`|Path to the file containing the predictions to load players in the database. <br/>Example : ``|

### Volume mounts
|Name|Description|
|:-:|---|
|`/data`|Contains data used by the API, such as the predictions file by default.|


### Full docker-compose example :
```
  lepooler-api:
    container_name: lepooler-api
    image: registry.gitlab.com/lepooler/lepooler-api:latest
    ports:
      - 2846:2846
    environment:
      - CORS_ALLOWED_ORIGINS=https://lepooler.example.com, http://lepooler.example.com
      - JWT_VALID_ISSUERS=lepoolerapi.example.com
      - JWT_VALID_AUDIENCES=lepooler.example.com
      - JWT_SIGNING_KEY=00000000-0000-0000-0000-000000000000
      - MYSQL_HOST=192.168.0.100
      - MYSQL_PORT=3306
      - MYSQL_DATABASE=lepooler
      - MYSQL_USER=lepooler
      - MYSQL_PASSWORD=SuperSecretPassword # You can also use Docker secrets : https://docs.docker.com/compose/compose-file/compose-file-v3/#secrets
      - PREDICTIONS_FILE_LOCATION=/data/predictions.json
    volumes:
      - /path/to/data:/data
    restart: unless-stopped
```
