﻿using LePooler.Data.Contexts;
using LePooler.Models.Poolers;
using LePooler.Models.Pools;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace LePooler.Data.Entities
{
    public class EntitiesLoader : IEntitiesLoader
    {
        private LePoolerContext _context;

        public EntitiesLoader(LePoolerContext context)
        {
            _context = context;
        }

        public void LoadPoolParticipants(Pool pool, bool loadPoolers = false, bool loadPicks = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false)
        {
            _context.Entry(pool).Collection(p => p.PoolParticipants).Load();

            if (loadPicks)
            {
                LoadPicks(pool, loadPoolers, loadPlayers, loadPlayersPredictions, loadInjuries);
            } 
            else if (loadPoolers)
            {
                foreach (var poolParticipant in pool.PoolParticipants)
                {
                    LoadPooler(poolParticipant);
                }
            }
        }

        public void LoadPoolParticipants(Guid poolGuid, bool loadPoolers = false, bool loadPicks = false, bool loadPlayers = false, bool loadPlayersPredictions = false)
        {
            LoadPoolParticipants(poolGuid, loadPoolers, loadPicks, loadPlayers, loadPlayersPredictions);
        }

        public void LoadPicks(Pool pool, bool loadPoolers = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false)
        {
            foreach (var poolParticipant in pool.PoolParticipants)
            {
                _context.Entry(poolParticipant).Collection(p => p.Picks).Load();
                if (loadPoolers)
                {
                    LoadPooler(poolParticipant);
                }

                if (loadPlayers)
                {
                    foreach (var pick in poolParticipant.Picks)
                    {
                        LoadPlayer(pick, loadPlayersPredictions, loadInjuries);
                    }
                }
            }
        }

        public void LoadPooler(Pick pick)
        {
            var contextEntry = _context.Entry(pick);
            if (contextEntry.State != EntityState.Detached)
            {
                contextEntry.Reference(p => p.PoolParticipant).Load();
                contextEntry.Reference(p => p.PoolParticipant.Pooler).Load();
            }
        }

        public void LoadPlayer(Pick pick, bool loadPlayersPredictions = false, bool loadInjuries = false)
        {
            var contextEntry = _context.Entry(pick);
            if (contextEntry.State == EntityState.Detached)
                return;

            contextEntry.Reference(p => p.Player).Load();
            if (loadPlayersPredictions)
            {
                _context.Entry(pick.Player).Collection(p => p.Predictions).Query().Where(p => p.Season == pick.PoolParticipant.Pool.Season).Load();
            }
                

            if (loadInjuries)
                _context.Entry(pick.Player).Collection(p => p.Injuries).Load();
        }

        public void LoadPooler(PoolParticipant poolParticipant)
        {
            var contextEntry = _context.Entry(poolParticipant);
            if (contextEntry.State == EntityState.Detached)
                return;

            contextEntry.Reference(p => p.Pooler).Load();
        }

        public Pooler LoadAndGetPooler(PoolParticipant poolParticipant)
        {
            LoadPooler(poolParticipant);
            return poolParticipant.Pooler;
        }
    }
}