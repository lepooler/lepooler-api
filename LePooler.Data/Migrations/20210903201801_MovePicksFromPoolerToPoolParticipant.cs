﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.Data.Migrations
{
    public partial class MovePicksFromPoolerToPoolParticipant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pick_Poolers_PoolerGuid",
                table: "Pick");

            migrationBuilder.DropIndex(
                name: "IX_Pick_PoolerGuid",
                table: "Pick");

            migrationBuilder.CreateIndex(
                name: "IX_Pick_PoolGuid_PoolerGuid",
                table: "Pick",
                columns: new[] { "PoolGuid", "PoolerGuid" });

            migrationBuilder.AddForeignKey(
                name: "FK_Pick_PoolParticipant_PoolGuid_PoolerGuid",
                table: "Pick",
                columns: new[] { "PoolGuid", "PoolerGuid" },
                principalTable: "PoolParticipant",
                principalColumns: new[] { "PoolGuid", "PoolerGuid" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pick_PoolParticipant_PoolGuid_PoolerGuid",
                table: "Pick");

            migrationBuilder.DropIndex(
                name: "IX_Pick_PoolGuid_PoolerGuid",
                table: "Pick");

            migrationBuilder.CreateIndex(
                name: "IX_Pick_PoolerGuid",
                table: "Pick",
                column: "PoolerGuid");

            migrationBuilder.AddForeignKey(
                name: "FK_Pick_Poolers_PoolerGuid",
                table: "Pick",
                column: "PoolerGuid",
                principalTable: "Poolers",
                principalColumn: "PoolerGuid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
