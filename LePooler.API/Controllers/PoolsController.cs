﻿using LePooler.Common.Configuration;
using LePooler.Common.Constants;
using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Logic.Pools.PlayerSelection;
using LePooler.Models.Authentication;
using LePooler.Models.Players;
using LePooler.Models.Poolers;
using LePooler.Models.Pools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;

namespace LePooler.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Policies.Authenticated)]
    public class PoolsController : BaseController
    {
        private const string GetPoolInfoPath = "info";

        private IPicksLogic _picksLogic;

        public PoolsController(LePoolerContext context, IEntitiesLoader entitiesLoader, IPicksLogic picksLogic) : base(context, entitiesLoader)
        {
            _picksLogic = picksLogic;
        }

        [Authorize("DevOnly")]
        [HttpGet]
        public ActionResult<IEnumerable<Pool>> Get() => Ok(Context.Pools);

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ActionResult<string> Authenticate([FromBody] TokenRequest request)
        {
            var pool = Context.Pools.SingleOrDefault(p => p.Name == request.PoolName && p.Season == request.Season);
            if (pool == null || !pool.PasswordMatches(request.Password))
                return BadRequest("Authentification échouée");

            return CreatePoolAuthenticationToken(HttpStatusCode.OK, pool, Policies.PoolAdmin);
        }

        [AllowAnonymous]
        [HttpGet("authenticateSpectating/{poolGuid}")]
        public ActionResult<string> AuthenticateSpectating(string poolGuid)
        {
            var pool = Context.Pools.SingleOrDefault(p => p.PoolGuid.ToString().Equals(poolGuid, StringComparison.OrdinalIgnoreCase));
            if (pool == null)
                return BadRequest("Lien invalide");

            return CreatePoolAuthenticationToken(HttpStatusCode.OK, pool, Policies.PoolSpectate);
        }

        [HttpGet(GetPoolInfoPath)]
        public ActionResult<Pool> GetPoolInfo()
        {
            var pool = GetAuthenticatedPool();
            return pool == null ? PoolDoesNotExist() : Ok(pool);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult<string> CreatePool([FromBody] Pool pool)
        {
            //Hash password, then drop it from memory
            pool.HashPassword();
            pool.Password = null;
            GC.Collect();

            if (pool.PoolGuid != Guid.Empty)
                return BadRequest("Le GUID devrait être vide afin de créer un nouveau pool");

            Common.Utilities.Validation.ValidateSeason(pool.Season);

            if (Context.Pools.Any(p => p.Name == pool.Name && p.Season == pool.Season))
                return Conflict("Un pool avec ce nom existe déjà pour cette saison");

            pool.PoolGuid = Guid.NewGuid();
            Context.Pools.Add(pool);
            var authenticationTokenReponse = CreatePoolAuthenticationToken(HttpStatusCode.Created, pool, Policies.PoolAdmin);
            var resultStatusCode = (authenticationTokenReponse.Result as IStatusCodeActionResult).StatusCode;
            if (resultStatusCode >= 200 && resultStatusCode < 300)
            {
                Context.SaveChanges();
            }
            return authenticationTokenReponse;
        }

        [HttpPut]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<Pool> UpdatePool([FromBody] Pool updatedPool)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();
            updatedPool.PoolGuid = pool.PoolGuid;

            LoadPoolParticipants(pool, loadPicks: true);
            if (pool.Picks.Any())
                return PlayerSelectionStarted();

            pool.Name = updatedPool.Name;
            pool.Season = updatedPool.Season;
            pool.NbGoalerPicks = updatedPool.NbGoalerPicks;
            pool.NbOffenseDefensePicks = updatedPool.NbOffenseDefensePicks;
            pool.PickTimeLimitInSeconds = updatedPool.PickTimeLimitInSeconds;
            Context.SaveChanges();
            return Ok(pool);
        }

        [HttpGet("poolParticipants")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<IEnumerable<PoolParticipant>> GetPoolParticipantsInPool()
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPoolers: true);
            return Ok(pool.PoolParticipants);
        }

        [HttpPost("poolParticipants")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<PoolParticipant> AddPoolParticipantToPool([FromBody] Pooler pooler)
        {
            if (Regex.IsMatch(pooler.Name, @"^\s+$"))
                return BadRequest("Le nom du pooler doit contenir au moins 1 caractère qui n'est pas un espace.");

            if (pooler.PoolerGuid == Guid.Empty)
            {
                pooler.PoolerGuid = Guid.NewGuid();
                Context.Poolers.Add(pooler);
            }

            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPoolers: true, loadPicks: true);
            if (pool.Picks.Any())
                return PlayerSelectionStarted();

            if (pool.PoolParticipants.Any(p => p.PoolerGuid == pooler.PoolerGuid))
                return Conflict("Ce pooler est déjà dans le pool");

            if (Context.Poolers.Find(pooler.PoolerGuid) == null)
                return Conflict("Le pooler ne doit pas avoir de GUID ou doit avoir un GUID existant");

            if (pool.PoolParticipants.Any(p => p.Pooler.Name == pooler.Name))
                return Conflict("Un pooler a déjà ce nom dans le pool");

            var nbPoolers = pool.PoolParticipants.Count;
            var poolParticipant = new PoolParticipant
            {
                PoolGuid = pool.PoolGuid,
                PoolerGuid = pooler.PoolerGuid,
                PickOrderPosition = nbPoolers + 1
            };

            pool.PoolParticipants.Add(poolParticipant);
            Context.SaveChanges();
            return Ok(poolParticipant);
        }

        [HttpDelete("poolParticipants/{poolerGuid}")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<Dictionary<int, Pooler>> DeletePoolParticipantFromPool(Guid poolerGuid)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPicks: true);
            if (pool.Picks.Any())
                return PlayerSelectionStarted();

            var poolParticipant = pool.PoolParticipants.SingleOrDefault(p => p.PoolGuid == pool.PoolGuid && p.PoolerGuid == poolerGuid);
            if (poolParticipant == null)
                return PoolerDoesNotExist();

            pool.PoolParticipants.Remove(poolParticipant);
            AdjustPickOrderPositions(pool.PoolParticipants.Count, poolParticipant.PickOrderPosition - 1,
                pool.PoolParticipants.OrderBy(p => p.PickOrderPosition).ToList(), moveOtherParticipantsUp: false);

            Context.SaveChanges();
            return Ok(_picksLogic.GetPoolParticipantsInPickOrder(pool));
        }

        [HttpGet("pickOrder")]
        public ActionResult<ICollection<PoolParticipant>> GetPoolParticipantsInPickOrder()
        {
            var pool = GetAuthenticatedPool();
            return pool == null ? PoolDoesNotExist() : Ok(_picksLogic.GetPoolParticipantsInPickOrder(pool));
        }

        [HttpPut("poolParticipants/{poolerGuid}/pickOrder")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<Dictionary<int, Pooler>> ChangePoolParticipantPickOrderPosition(Guid poolerGuid, [FromBody] int newPickOrderPosition)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            if (newPickOrderPosition < 1)
                newPickOrderPosition = 1;

            LoadPoolParticipants(pool, loadPicks: true);
            if (pool.Picks.Any())
                return PlayerSelectionStarted();

            var nbPoolers = pool.PoolParticipants.Count;
            if (newPickOrderPosition > nbPoolers)
                newPickOrderPosition = nbPoolers;

            var poolParticipantsInPickOrder = pool.PoolParticipants.OrderBy(p => p.PickOrderPosition).ToList();
            var poolParticipantToMove = poolParticipantsInPickOrder.SingleOrDefault(p => p.PoolerGuid == poolerGuid);
            if (poolParticipantToMove == null)
                return PoolerDoesNotExist();

            if (poolParticipantToMove.PickOrderPosition == newPickOrderPosition)
                return BadRequest("Le nouvel ordre de sélection doit être différent de l'ordre de sélection actuel");

            var moveOtherParticipantsUp = newPickOrderPosition < poolParticipantToMove.PickOrderPosition;
            AdjustPickOrderPositions(newPickOrderPosition, poolParticipantToMove.PickOrderPosition, poolParticipantsInPickOrder, moveOtherParticipantsUp);

            poolParticipantToMove.PickOrderPosition = newPickOrderPosition;
            Context.SaveChanges();
            return Ok(_picksLogic.GetPoolParticipantsInPickOrder(pool));
        }

        [HttpGet("nextPoolParticipantsToPick/{*nbNextPicks}")]
        public ActionResult<Dictionary<int, Guid>> GetNextPoolParticipantsToPick(int? nbNextPicks)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            var nextPoolersToPick = _picksLogic.GetNextPoolParticipantsToPick(nbNextPicks, pool);

            return Ok(nextPoolersToPick);
        }

        [HttpGet("poolParticipants/{poolerGuid}/picks")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<IEnumerable<Player>> GetPicksForPoolParticipant(Guid poolerGuid)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool);
            var poolParticipant = pool.PoolParticipants.SingleOrDefault(p => p.PoolGuid == pool.PoolGuid && p.PoolerGuid == poolerGuid);
            if (poolParticipant == null)
                return PoolerDoesNotExist();

            LoadPicks(pool, loadPlayers: true, loadPlayersPredictions: true, loadInjuries: true);
            return Ok(pool.GetPoolerPicks(poolerGuid));
        }

        [HttpGet("picks")]
        public ActionResult<Dictionary<Guid, IEnumerable<Player>>> GetPicks()
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPicks: true, loadPlayers: true, loadPlayersPredictions: true, loadInjuries: true);
            var picksByPlayer = pool.PoolParticipants.Select(p => p.PoolerGuid).ToDictionary(poolerGuid => poolerGuid, poolerGuid => pool.GetPoolerPicks(poolerGuid));
            return Ok(picksByPlayer);
        }

        [HttpPost("picks")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<KeyValuePair<Guid, IEnumerable<Player>>> PickPlayer([FromBody] string playerNhlId)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool);
            if (pool.PoolParticipants.Count == 0)
                return Conflict(
                    "Au moins un pooler doit être ajouté au pool avant de commencer la sélection des joueurs");

            var playerToPick = Context.Players.Find(playerNhlId);
            if (playerToPick == null)
                return DoesNotExist("Player");

            LoadPoolParticipants(pool, loadPicks: true, loadPlayers: true, loadPlayersPredictions: true, loadInjuries: true);

            try
            {
                var pick = pool.PickPlayer(playerToPick);
                Context.SaveChanges();
                LoadPlayer(pick, true, true);
                return Ok(new KeyValuePair<Guid, IEnumerable<Player>>(pick.PoolerGuid, pool.GetPoolerPicks(pick.PoolerGuid)));
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(ex.Message);
            }
        }

        [HttpDelete("picks")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<KeyValuePair<Guid, IEnumerable<Player>>> UndoLastPick()
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPicks: true, loadPlayers: true, loadPlayersPredictions: true, loadInjuries: true);
            if (pool.LastPick == null)
                return Conflict("Il n'y a pas de choix à annuler.");

            var lastPickPooler = pool.LastPick.PoolerGuid;
            pool.UndoLastPick();
            Context.SaveChanges();
            return Ok(new KeyValuePair<Guid, IEnumerable<Player>>(lastPickPooler, pool.GetPoolerPicks(lastPickPooler)));
        }

        [HttpGet("playerSelectionStarted")]
        [Authorize(Policies.PoolAdmin)]
        public ActionResult<bool> GetPlayerSelectionStarted()
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();

            LoadPoolParticipants(pool, loadPicks: true);

            return Ok(pool.Picks.Any());
        }

        private ActionResult<string> CreatePoolAuthenticationToken(HttpStatusCode code, Pool pool, string claim)
        {
            if (!HttpContext.Request.Headers.ContainsKey("Origin")) return BadRequest("Un en-tête \"Origin\" doit être fourni");

            var audience = new Regex("https?://").Replace(HttpContext.Request.Headers["Origin"].ToString(), "");
            if (!AppConfiguration.JwtValidAudiences.Any(a => a.Equals(audience))) return BadRequest("Audience invalide");

            var claims = new[]
            {
                new Claim(Common.Constants.ClaimTypes.PoolGuid, pool.PoolGuid.ToString()),
                new Claim(claim, pool.PoolGuid.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppConfiguration.JwtSigningKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: HttpContext.Request.Host.Host,
                audience: audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(AuthenticationToken.DurationInMinutes),
                signingCredentials: creds);
            var resultToken = new JwtSecurityTokenHandler().WriteToken(token);

            if (code == HttpStatusCode.Created)
            {
                return Created(Request.Path + GetPoolInfoPath, resultToken);
            }

            return StatusCode((int)code, resultToken);
        }

        private static void AdjustPickOrderPositions(int firstPositionToAdjust, int stopAdjustingPosition, IReadOnlyList<PoolParticipant> poolParticipantsInPickOrder,
            bool moveOtherParticipantsUp)
        {
            var moveParticipantsBy = moveOtherParticipantsUp ? 1 : -1;
            for (var i = firstPositionToAdjust;
                (moveOtherParticipantsUp && i < stopAdjustingPosition) || (!moveOtherParticipantsUp && i > stopAdjustingPosition);
                i += moveParticipantsBy)
            {
                poolParticipantsInPickOrder[i - 1].PickOrderPosition += moveParticipantsBy;
            }
        }

        private Pooler LoadAndGetPooler(PoolParticipant poolParticipant)
        {
            LoadPooler(poolParticipant);
            return poolParticipant.Pooler;
        }

        private ConflictObjectResult PlayerSelectionStarted()
        {
            return Conflict("Cette action n'est plus possible pour ce pool puisque la sélection des joueurs a débutée");
        }
    }
}