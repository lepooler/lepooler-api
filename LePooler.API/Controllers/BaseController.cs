﻿using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Models.Pools;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace LePooler.API.Controllers
{
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        protected LePoolerContext Context { get; }
        protected IEntitiesLoader EntitiesLoader { get; }

        protected BaseController(LePoolerContext context, IEntitiesLoader entitiesLoader)
        {
            Context = context;
            EntitiesLoader = entitiesLoader;
        }

        protected ActionResult PoolDoesNotExist()
        {
            return DoesNotExist("pool");
        }

        protected ActionResult PoolerDoesNotExist()
        {
            return DoesNotExist("pooler");
        }

        protected ActionResult DoesNotExist(string objectName)
        {
            return NotFound("Ce " + objectName + " n'existe pas.");
        }

        protected string GetPoolGuidClaim()
        {
            return Common.Utilities.Authentication.GetPoolGuidClaim(User.Identity);
        }

        protected Pool GetAuthenticatedPool()
        {
            return Context.Pools.SingleOrDefault(p => p.PoolGuid.ToString() == GetPoolGuidClaim());
        }

        protected void LoadPoolParticipants(Pool pool, bool loadPoolers = false, bool loadPicks = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false)
        {
            EntitiesLoader.LoadPoolParticipants(pool, loadPoolers, loadPicks, loadPlayers, loadPlayersPredictions, loadInjuries);
        }

        protected void LoadPicks(Pool pool, bool loadPoolers = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false)
        {
            EntitiesLoader.LoadPicks(pool, loadPoolers, loadPlayers, loadPlayersPredictions, loadInjuries);
        }

        protected void LoadPooler(Pick pick)
        {
            EntitiesLoader.LoadPooler(pick);
        }

        protected void LoadPlayer(Pick pick, bool loadPlayersPredictions = false, bool loadInjuries = false )
        {
            EntitiesLoader.LoadPlayer(pick, loadPlayersPredictions, loadInjuries);
        }

        protected void LoadPooler(PoolParticipant poolParticipant)
        {
            EntitiesLoader.LoadPooler(poolParticipant);
        }
    }
}