﻿using LePooler.Common.Constants;
using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Models.Poolers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace LePooler.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Policies.PoolAdmin)]
    public class PoolersController : BaseController
    {
        public PoolersController(LePoolerContext context, IEntitiesLoader entitiesLoader) : base(context, entitiesLoader)
        {
        }

        [HttpPut()]
        public ActionResult<Pooler> UpdatePooler([FromBody] Pooler updatedPooler)
        {
            var pool = GetAuthenticatedPool();
            if (pool == null)
                return PoolDoesNotExist();
            LoadPoolParticipants(pool, loadPoolers: true);

            var pooler = Context.Poolers.Find(updatedPooler.PoolerGuid);
            if (pooler == null || pool.PoolParticipants.All(p => p.PoolerGuid != pooler.PoolerGuid))
                return PoolerDoesNotExist();

            if (!Context.Entry(pooler).Entity.Name.Equals(updatedPooler.Name, StringComparison.InvariantCulture) && pool.PoolParticipants.Any(p => p.Pooler.Name == updatedPooler.Name))
                return Conflict("Un pooler a déjà ce nom dans le pool");

            Context.Entry(pooler).CurrentValues.SetValues(updatedPooler);
            Context.SaveChanges();
            Context.Entry(pooler).Collection(p => p.Pools).Load();
            return Ok(pooler);
        }
    }
}