﻿using LePooler.Common.Extensions;
using LePooler.Data.Contexts;
using LePooler.Models.Players;
using LePooler.Scrapers.Interfaces;
using LePooler.Scrapers.Outputs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LePooler.API.BackgroundServices
{
    public class UpdateInjuriesBackgroundService : BackgroundService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<UpdateInjuriesBackgroundService> _logger;

        public UpdateInjuriesBackgroundService(IServiceScopeFactory scopeFactory, ILogger<UpdateInjuriesBackgroundService> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                stoppingToken.Register(() => _logger.LogInformation($"Background task UpdateInjuries is stopping."));

                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation($"Background task UpdateInjuries doing work.");
                    using (var scope = _scopeFactory.CreateScope())
                    {
                        var dbContext = scope.ServiceProvider.GetRequiredService<LePoolerContext>();

                        var currentInjuries = dbContext.Players.SelectMany(p => p.Injuries).Where(i => !i.EndDate.HasValue);
                        var scrapedInjuries = await scope.ServiceProvider.GetRequiredService<IInjuriesScraper>().GetInjuries();
                        var injuries = scrapedInjuries.Select(i => ToInjury(i, dbContext.Players.ToList())).Where(i => i != null).ToList();

                        // Update current injuries
                        foreach (var currentInjury in currentInjuries)
                        {
                            var newInjuryData = injuries.SingleOrDefault(i => i.PlayerNhlId == currentInjury.PlayerNhlId && (i.StartDate == DateTime.MinValue || i.StartDate == currentInjury.StartDate));
                            if (newInjuryData == null)
                            {
                                // End current injury if they are no longer in the list
                                currentInjury.EndDate = DateTime.Today.AddDays(-1);
                            }
                            else
                            {
                                // If scraper does not get a start date, use the current injury's date as start date
                                newInjuryData.StartDate = currentInjury.StartDate > newInjuryData.StartDate ? currentInjury.StartDate : newInjuryData.StartDate;
                                dbContext.Entry(currentInjury).CurrentValues.SetValues(newInjuryData);
                                injuries.Remove(newInjuryData);
                            }
                        }

                        // Add new injuries
                        foreach (var injury in injuries)
                        {
                            if (injury.Player.Injuries.All(i => i.EndDate.HasValue))
                            {
                                injury.StartDate = DateTime.Today;
                                injury.Player.Injuries.Add(injury);
                            }
                        }

                        await dbContext.SaveChangesAsync();
                    }

                    _logger.LogInformation($"Background task UpdateInjuries completed. Waiting until next run.");
                    await Task.Delay(TimeSpan.FromDays(1), stoppingToken);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }

        private Injury ToInjury(ScrapedInjury scrapedInjury, ICollection<Player> players)
        {
            var matchingPlayers = players
                .Where(p => p.Name.RemoveAccents().Equals(scrapedInjury.PlayerName.RemoveAccents(), StringComparison.InvariantCultureIgnoreCase) &&
                                        p.Team.RemoveAccents().Equals(scrapedInjury.TeamName.RemoveAccents(), StringComparison.InvariantCultureIgnoreCase));
            var identifiedPlayer = matchingPlayers.FirstOrDefault(p => p.PlayerNhlId.All(char.IsNumber));
            if (identifiedPlayer == null)
            {
                identifiedPlayer = matchingPlayers.FirstOrDefault();
            }

            if (identifiedPlayer != null)
            {
                return new Injury()
                {
                    Player = identifiedPlayer,
                    PlayerNhlId = identifiedPlayer.PlayerNhlId,
                    StartDate = scrapedInjury.StartDate,
                    EndDate = scrapedInjury.EndDate,
                    Description = scrapedInjury.Description
                };
            }
            return null;
        }
    }
}