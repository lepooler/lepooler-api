﻿namespace LePooler.Common.Constants
{
    public static class Policies
    {
        public const string Authenticated = "Authenticated";
        public const string PoolAdmin = "PoolAdmin";
        public const string PoolSpectate = "PoolSpectate";
    }
}