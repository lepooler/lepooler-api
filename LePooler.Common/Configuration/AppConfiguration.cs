using System;
using System.Collections.Generic;
using System.Linq;

namespace LePooler.Common.Configuration
{
    public static class AppConfiguration
    {
        private const string CorsAllowedOriginsEnvironmentVariableName = "CORS_ALLOWED_ORIGINS";

        private const string JwtValidIssuersEnvironmentVariableName = "JWT_VALID_ISSUERS";
        private const string JwtValidAudiencesEnvironmentVariableName = "JWT_VALID_AUDIENCES";
        private const string JwtSigningKeyEnvironmentVariableName = "JWT_SIGNING_KEY";

        private const string MySqlHostEnvironmentVariableName = "MYSQL_HOST";
        private const string MySqlPortEnvironmentVariableName = "MYSQL_PORT";
        private const string MySqlDatabaseEnvironmentVariableName = "MYSQL_DATABASE";
        private const string MySqlUserEnvironmentVariableName = "MYSQL_USER";
        private const string MySqlPasswordEnvironmentVariableName = "MYSQL_PASSWORD";

        private const string PredictionsFileLocationEnvironmentVariableName = "PREDICTIONS_FILE_LOCATION";
        private const string DefaultPredictionsFileLocation = "/data/predictions.json";

        public static IEnumerable<string> CorsAllowedOrigins => Environment.GetEnvironmentVariable(CorsAllowedOriginsEnvironmentVariableName).Split(',').Select(x => x.Trim());

        public static IEnumerable<string> JwtValidIssuers => Environment.GetEnvironmentVariable(JwtValidIssuersEnvironmentVariableName).Split(',').Select(x => x.Trim());
        public static IEnumerable<string> JwtValidAudiences => Environment.GetEnvironmentVariable(JwtValidAudiencesEnvironmentVariableName).Split(',').Select(x => x.Trim());
        public static string JwtSigningKey => Environment.GetEnvironmentVariable(JwtSigningKeyEnvironmentVariableName);

        public static string MySqlHost => Environment.GetEnvironmentVariable(MySqlHostEnvironmentVariableName);
        public static string MySqlPort => Environment.GetEnvironmentVariable(MySqlPortEnvironmentVariableName);
        public static string MySqlDatabase => Environment.GetEnvironmentVariable(MySqlDatabaseEnvironmentVariableName);

        public static string MySqlUser => Environment.GetEnvironmentVariable(MySqlUserEnvironmentVariableName);
        public static string MySqlPassword => Environment.GetEnvironmentVariable(MySqlPasswordEnvironmentVariableName);

        public static string PredictionsFileLocation => Environment.GetEnvironmentVariable(PredictionsFileLocationEnvironmentVariableName) ?? DefaultPredictionsFileLocation;
    }
}
