﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace LePooler.Common.Utilities
{
    public static class Authentication
    {
        public static string GetPoolGuidClaim(IIdentity identity)
        {
            return ((ClaimsIdentity)identity).Claims.Single(c => c.Type == Constants.ClaimTypes.PoolGuid).Value;
        }
    }
}