﻿using System;
using System.Text.RegularExpressions;

namespace LePooler.Common.Utilities
{
    public static class Validation
    {
        public static void ValidateSeason(string season)
        {
            if (!Regex.IsMatch(season, "\\d{4}-\\d{4}") || int.Parse(season.Substring(0, 4)) + 1 != int.Parse(season.Substring(5, 4)))
                throw new ArgumentException("La saison est invalide");
        }
    }
}